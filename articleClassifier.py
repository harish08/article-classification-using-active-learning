import io
import os
from keyWords import *
from classifierHelper import *




#retrieve keywords for labelled training articles
articles = os.listdir("Dataset/Train/Labelled/Sports")
for article in articles:
    print 'Reading Labelled Training Article ' + article
    File = io.open('Dataset/Train/Labelled/Sports/' + article, 'r')
    content = File.read()
    keyWords = getKeywords(content)
    keyWords= process(keyWords)
    #removes duplicate words from list
    keyWords= list(set(keyWords))
    addToKnowledge(knowledgebase,keyWords,0)
    

#labelled articles for the second class   
articles = os.listdir("Dataset/Train/Labelled/Business")
for article in articles:
    print 'Reading Labelled training article ' + article
    File = io.open('Dataset/Train/Labelled/Business/' + article, 'r')
    content = File.read()
    keyWords = getKeywords(content)
    keyWords= process(keyWords)
    keyWords= list(set(keyWords))
    addToKnowledge(knowledgebase,keyWords,1)


#code to deal  with unlabelled training data
j=0
while(j<3):
	articles = os.listdir("Dataset/Train/Unlabelled")
	for article in articles:
		print 'Reading unlabelled training article' + article
		File = io.open('Dataset/Train/Unlabelled/' + article, 'r')
		content = File.read()
		keyWords = getKeywords(content)
		keyWords= process(keyWords)
		keyWords= list(set(keyWords))
		articleNumber= int(article[:-4])
		prob= getProb(knowledgebase,keyWords)

		#probability threshold for classificaion
		if(prob<0.65 and prob >0.35):			
			label=getLabel(articleNumber)
			addToKnowledge(knowledgebase,keyWords,label)
			trainingLabels[articleNumber-1]=label
			
		elif(prob <=0.35):
			trainingLabels[articleNumber-1]=0
		else:
			trainingLabels[articleNumber-1]=1
	 
	class0=0
	class1=0
	i=0
	while(i<len(trainingLabels)):
		if(i%2==0 and trainingLabels[i]==0):
			class0+=1
		if(i%2!=0 and trainingLabels[i]==1):
			class1+=1
		i+=1

	j+=1
	
	print(class0)
	print(class1)
	
	
#code to deal  with test data
articles = os.listdir("Dataset/Test")
for article in articles:
	print 'Reading test article ' + article
	File = io.open('Dataset/Test/' + article, 'r')
	content = File.read()
	keyWords = getKeywords(content)
	keyWords= process(keyWords)
	articleNumber= int(article[:-4])
	prob= getTestProb(knowledgebase,keyWords)
	keyWords= list(set(keyWords))
	
	#probability threshold for classificaion	
	if(prob <=0.5):
		testLabels[articleNumber-1]=0
	else:
		testLabels[articleNumber-1]=1
	 
class0=0
class1=0
i=0
while(i<len(testLabels)):
	if(i%2==0 and testLabels[i]==0):
		class0+=1
	if(i%2!=0 and testLabels[i]==1):
		class1+=1
	i+=1
print(class0)
print(class1)	
print('Articles classified.')
queryArticle(testLabels)
