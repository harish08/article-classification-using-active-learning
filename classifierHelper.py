import io
import os
from keyWords import *

#global variables
knowledgebase={}
trainingLabels = [-1] * 50
testLabels = [-1] * 100

def getLabel(article):
	temp = True
	while temp:
		print("Enter 0 for Sports article and 1 for Business article")
		print("Enter label for ", article)
		query = raw_input(":")
		if(query == "1"):
			print("Label 1 added")
			temp = False
		elif(query == "0"):
			print("Label 0 added")
			temp = False
		else:
			print("Label 0 or 1")
	int(query)
	return(query)
	
	
def queryArticle(labels):
	temp = True
	while temp:
		print("Enter article number from 1-100 ")
		query = int(raw_input(":"))
		if(query >100 or query< 1):
			print("Enter number from 1-100 only")
			continue
		else:
			if(labels[query-1]==1):
				print("Business")
			else:
				print("Sports")
   
def toLower(list1):
	count = len(list1)
	for i in range(0,count):
		list1[i] = list1[i].lower()
	return list1
    
#Recreates new list by splitting the double word keywords that were treated as single word.
def split(list1):
    newList = []
    count = len(list1)
    for i in range(0,count):
        temp = list1[i].split(' ')
        tempCount = len(temp)
        for j in range(0,tempCount):
            newList.append(temp[j])
    return newList


#Remove special symbols from the keywords
def removeSymbols(list1):
    count = len(list1)
    for i in range(0,count):
        list1[i] = list1[i].replace("-","")
    for i in range(0,count):
        tempLength = len(list1[i])
        if (list1[i][tempLength-2] == '\'' and list1[i][tempLength-1] == 's'):
            list1[i] = list1[i][:-2]
    return list1
    
    
def process(words):
	words=toLower(words)
	words=split(words)
	words=removeSymbols(words)
	return words
    
    
def addToKnowledge(dic,keyWords,label):
	for i in keyWords:
		#check if the value is already in the list. If its label changes, mark the 
		#label as -1 indicating that it will no longer be used for classification.
		if i in dic:
			if(dic[i]!=label):
				dic[i]=int(-1)
		else:
			dic[i]=int(label)
			
#prob 1.0 => class 1			
def getProb(dic,keyWords):
	count0=0
	count1=0
	prob=0.5
	for word in keyWords:
		if word in knowledgebase:
			if(knowledgebase.get(word)==1):
				count1+=1
			if(knowledgebase.get(word)<=0.1 and knowledgebase.get(word)>=-0.1):
				count0+=1
		
		
	#if less matches are found, get the article's label and add that to knowledgebase
	if(count1+count0 < 3):
		prob=0.5
	else:
		if(count1!=0 and count0!=0 and (count0+count1)==3):
			prob=0.5
		else:
			prob=count1*1.0/(count0+count1)
	return prob
	
	
	

def getTestProb(dic,keyWords):
	count0=0
	count1=0
	prob=0.5
	for word in keyWords:
		if word in knowledgebase:
			if(knowledgebase.get(word)==1):
				count1+=1
			if(knowledgebase.get(word)<=0.1 and knowledgebase.get(word)>=-0.1):
				count0+=1
		
	#if less matches are found, get the article's label and add that to knowledgebase
	if(count1==0 and count0==0):
		return prob
	else:
		prob=count1*1.0/(count0+count1)
		return prob



	
